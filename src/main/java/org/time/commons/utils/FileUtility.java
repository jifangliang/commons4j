package org.time.commons.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileUtility extends FileUtils {

    protected static final Log log = LogFactory.getLog(FileUtility.class);

    public static String getMD5(File file) {
        try {
            return DigestUtility.md5Hex(FileUtility.readFileToByteArray(file));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String splicePath(String... paths) {
        String fullPath = null;
        for (String path : paths) {
            path = path.replaceAll("\\\\", "/");
            if (fullPath == null) {
                fullPath = path;
            } else if (fullPath.endsWith("/")) {
                fullPath += (path.startsWith("/") ? path.substring(1) : path);
            } else {
                fullPath += (path.startsWith("/") ? path : ("/" + path));
            }
        }
        return fullPath;
    }

    public static File createTempDir(String prefix) {
        try {
            File dir = File.createTempFile(prefix, "dir");
            if (dir.delete()) {
                if (dir.mkdir()) { return dir; }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<File> unZip(ZipFile zipFile, File dir, String fileNameEncode) {
        List<File> files = new ArrayList<File>();
        try {
            Enumeration<ZipArchiveEntry> enumeration = zipFile.getEntries();
            while (enumeration.hasMoreElements()) {
                ZipArchiveEntry entry = enumeration.nextElement();
                String fileName = new String(entry.getRawName(), fileNameEncode);
                if (!fileName.endsWith("/")) {
                    File outFile = new File(dir, fileName);
                    InputStream io = zipFile.getInputStream(entry);
                    outFile.getParentFile().mkdirs();
                    OutputStream output = new FileOutputStream(outFile);
                    IOUtils.copy(io, output);
                    files.add(outFile);
                    IOUtils.closeQuietly(io);
                    IOUtils.closeQuietly(output);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return files;
    }
}
