/*
 * Copyright (c) 2010-2014 time All Rights Reserved
 *
 * File:RuntimeSQLException.java Project: Commons4j
 * 
 * Creator:time
 * Mail:jifangliang@163.com
 * Date:2014年1月24日 下午5:02:36
 * 
 */
package org.time.commons.tools;

import java.sql.SQLException;

/**
 * @author JFL
 *
 */
public class RuntimeSQLException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public RuntimeSQLException(String msg, SQLException e) {
        super(msg, e);
    }
}
