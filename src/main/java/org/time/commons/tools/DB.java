/*
 * Copyright (c) 2010-2014 time All Rights Reserved
 *
 * File:DB.java Project: Commons4j
 * 
 * Creator:time
 * Mail:jifangliang@163.com
 * Date:2014年1月24日 下午5:03:15
 * 
 */
package org.time.commons.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

/**
 * @author JFL
 *
 */
public class DB {
    private String url = null;
    private String user = null;
    private String password = null;

    private Connection transactionConn;

    static {
        DbUtils.loadDriver(Config.getInstance().getString("jdbc.driverClassName"));
    }

    public DB() {
        this.url = Config.getInstance().getString("jdbc.url");
        this.user = Config.getInstance().getString("jdbc.username");
        this.password = Config.getInstance().getString("jdbc.password");
    }

    private Connection getConnection() {
        try {
            if (transactionConn != null) {
                return transactionConn;
            } else {
                return DriverManager.getConnection(url, user, password);
            }
        } catch (SQLException e) {
            throw new RuntimeSQLException("数据库连接获取失败", e);
        }
    }

    private void close(Connection conn) {
        if (transactionConn == conn) { return; }
        DbUtils.closeQuietly(conn);
    }

    public void beginTransaction() {
        this.transactionConn = getConnection();
        try {
            this.transactionConn.setAutoCommit(false);
        } catch (SQLException e) {
            throw new RuntimeSQLException("数据库连接打开事务失败", e);
        }
    }

    public void commitTransaction() {
        DbUtils.commitAndCloseQuietly(this.transactionConn);
        this.transactionConn = null;
    }

    public void rollbackTransaction() {
        DbUtils.rollbackAndCloseQuietly(this.transactionConn);
        this.transactionConn = null;
    }

    public <T> T queryObject(String sql, Class<T> cls, Object... params) {
        List<T> list = queryObjectList(sql, cls, params);
        return list.size() > 0 ? list.get(0) : null;
    }

    public <T> List<T> queryObjectList(String sql, Class<T> cls, Object... params) {
        Connection conn = getConnection();
        try {
            return new QueryRunner().query(conn, sql, new ColumnListHandler<T>(1), params);
        } catch (SQLException e) {
            throw new RuntimeSQLException("SQL执行失败 -> " + sql, e);
        } finally {
            close(conn);
        }
    }

    public <T> T queryBean(String sql, Class<T> beanClass, Object... params) {
        List<T> list = queryBeanList(sql, beanClass, params);
        return list.size() > 0 ? list.get(0) : null;
    }

    public <T> List<T> queryBeanList(String sql, Class<T> beanClass, Object... params) {
        Connection conn = getConnection();
        try {
            return new QueryRunner().query(conn, sql, new BeanListHandler<T>(beanClass), params);
        } catch (SQLException e) {
            throw new RuntimeSQLException("SQL执行失败 -> " + sql, e);
        } finally {
            close(conn);
        }
    }

    public Map<String, Object> queryMap(String sql, Object... params) {
        List<Map<String, Object>> list = queryMapList(sql, params);
        return list.size() > 0 ? list.get(0) : null;
    }

    public List<Map<String, Object>> queryMapList(String sql, Object... params) {
        Connection conn = getConnection();
        try {
            return new QueryRunner().query(conn, sql, new MapListHandler(), params);
        } catch (SQLException e) {
            throw new RuntimeSQLException("SQL执行失败 -> " + sql, e);
        } finally {
            close(conn);
        }
    }

    public int update(String sql, Object... params) {
        Connection conn = getConnection();
        try {
            return new QueryRunner().update(conn, sql, params);
        } catch (SQLException e) {
            throw new RuntimeSQLException("SQL执行失败 -> " + sql, e);
        } finally {
            close(conn);
        }
    }

    public long insert(String sql, Object... params) {
        Connection conn = getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.executeUpdate();
            rs = stmt.getGeneratedKeys();

            if (rs.next()) return rs.getLong(1);
            else return 0L;

        } catch (SQLException e) {
            throw new RuntimeSQLException("SQL执行失败 -> " + sql, e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(stmt);
            close(conn);
        }
    }
}
